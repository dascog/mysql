/* At the beginning load up the SQLDBScriptsMySQL\NorthwindDB.sql and run it - takes a while to load */

DROP DATABASE IF EXISTS DemoDB;

CREATE DATABASE DemoDB;		-- CREATE DATABASE (or CREATE SCHEMA) will create a new schema/database in your list
USE DemoDB; 				-- this switches the focus to the new database. Othewise you have to use dbname.tablename to reference
SHOW DATABASES; 			-- show all the current databases (can include some you don't see in your schema

-- Create a new table in your selected schema. All commands finish with a ;. THe column  names in the create command must be comma separated.
CREATE TABLE Employees (
	Name VARCHAR(20),
    Position VARCHAR(25),
    Salary INT
);

SHOW TABLES;			-- show all the tables in your current database (just one!)

-- PRIMARY KEY CONSTRAINT: Create a table with a primary key - must be unique for all rows
-- First way - this means the system creates an index name for the Primary Key itself
CREATE TABLE Contact 
(
	ContactID INT AUTO_INCREMENT PRIMARY KEY, -- PRIMARY KEY says this is the primary key for the table, AUTO_INCREMENT means the system will supply the values
    LastName VARCHAR(50) NOT NULL, -- have to have this
    FirstName VARCHAR(50),
    City VARCHAR(50)
);
DROP TABLE Contact;	-- this is how you remove a table

-- alternative creates an explicit constraint name too
CREATE TABLE Contact (
    ContactID INT AUTO_INCREMENT NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    FirstName VARCHAR(50),
    Address VARCHAR(50),
    City VARCHAR(50),
    CONSTRAINT pk_contact PRIMARY KEY (ContactID)
);
SHOW INDEXES FROM Contact;
-- suppose you want to drop the primary key? You could try this ...
ALTER TABLE Contact DROP PRIMARY KEY;
-- ...but it won't work, because it is an auto-increment column. You have to change the type of the primary key before dropping it
ALTER TABLE Contact DROP PRIMARY KEY, MODIFY ContactID INT;
SHOW COLUMNS FROM Contact;

-- re-add the primary key
ALTER TABLE Contact 
ADD PRIMARY KEY (ContactID);
SHOW COLUMNS FROM Contact;

-- you can also reset the AUTO_INCREMENT
ALTER TABLE Contact MODIFY ContactID INT AUTO_INCREMENT;
SHOW COLUMNS FROM Contact;

DROP TABLE Contact;

/* Foreign Key Constraints */
-- first create another table for the country of the contact

CREATE TABLE Country 
(
	CountryID INT AUTO_INCREMENT PRIMARY KEY,
    CountryName VARCHAR(50),
    Continent VARCHAR(20),
    Capital VARCHAR(50)
);

CREATE TABLE Contact
(
	ContactID int AUTO_INCREMENT PRIMARY KEY,
	LastName varchar(50) NOT NULL,
	FirstName varchar(50),
	Address varchar(50),
	City varchar(50),
	CountryID int,
	FOREIGN KEY (CountryID) REFERENCES Country(CountryID)
);
-- OR
CREATE TABLE Contact (
    ContactID INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    LastName VARCHAR(50) NOT NULL,
    FirstName VARCHAR(50),
    Address VARCHAR(50),
    City VARCHAR(50),
    CountryID INT NOT NULL,
    CONSTRAINT fk_contact_country FOREIGN KEY (CountryID)
        REFERENCES Country (CountryID)
);

-- You can also constrain a column to have unique row entries
-- in the CREATE call:
DROP TABLE Contact;
CREATE TABLE Contact
(
	ContactID int AUTO_INCREMENT PRIMARY KEY,
	Name varchar(50) NOT NULL,
	EmailAddress varchar(50) UNIQUE NOT NULL,
	City varchar(50),
    CountryID int,
	FOREIGN KEY (CountryID) REFERENCES Country(CountryID)
);

-- let's try it out
DELETE FROM Country WHERE CountryID > 1;
ALTER TABLE Country AUTO_INCREMENT=1;

INSERT INTO Country(CountryName,Continent,Capital) VALUES ('Australia','Oceania','Canberra');
SELECT * FROM Country;

INSERT INTO Contact(Name,EmailAddress,CountryID) VALUES ('Darryl','dsgreig@gmail.com',1);
SELECT * FROM Contact;

DELETE FROM Country WHERE CountryID=1; -- Won't work!
DELETE FROM Contact WHERE CountryID=1;
SELECT * FROM Contact;
-- we can populate one database from another!
INSERT INTO Country(CountryName, Continent) SELECT Name, Continent FROM world.country;
SELECT * FROM Country;

USE World;
SELECT * FROM Country;

/* Switch the the Northwind database */
USE Northwind;
SELECT * FROM Shippers;
INSERT Shippers(CompanyName, Phone) VALUES('AllIrish Stout','23456789');
UPDATE Shippers SET Phone = '99885671234' WHERE ShipperID = 4;
DELETE FROM Shippers WHERE ShipperID = 4;
/* NOTE that if you try to update or delete rows based on something other than a key, it will fail because MySQL automatically 
implements a "safe mode" to enforce this - this can be disabled in the workbench 
by going to Edit > Preferences > SQL Editor, the toggling the preference at the bottom and reconnecting */

-- More DELETE (this doesn't work for the same reason as above - then it doesn't work because it violates the foreign key constraint)
SELECT * FROM orders;
DELETE FROM orders WHERE YEAR(OrderDate) = 2016; 

-- UPDATE
SELECT ProductName, UnitPrice FROM Products where CategoryID = 5;
UPDATE products
	SET UnitPrice = 1.1*(UnitPrice) 
    WHERE CategoryID = 5;
SELECT ProductName, UnitPrice FROM Products where CategoryID = 5;

-- SELECT Statements
SELECT CompanyName AS Supplier, ContactName Contact, Country, City FROM Suppliers
WHERE Country = 'Germany' OR Country = 'France';

SELECT CompanyName AS Supplier, ContactName Contact, Country, City FROM Suppliers
WHERE Country NOT IN('Germany','France','Spain');

SELECT CompanyName AS Supplier, ContactName Contact, Country, City FROM Suppliers
WHERE Country <> 'Germany';

SELECT OrderID, OrderDate FROM Orders WHERE OrderDate <= '2017-01-01';

SELECT OrderID, OrderDate FROM Orders WHERE OrderDate < '2016-09-01'
AND OrderDate > '2016-07-31';

SELECT OrderID, OrderDate FROM Orders WHERE OrderDate BETWEEN '2016-07-31'
AND '2016-09-01';

SELECT OrderID, OrderDate FROM Orders WHERE YEAR(OrderDate) = 2016
AND MONTH(OrderDate) = 8;

SELECT CompanyName FROM Customers WHERE CompanyName LIKE '%Market%';

SELECT CustomerID, CompanyName FROM Customers WHERE CustomerID LIKE '_N___';

INSERT Shippers(CompanyName, Phone) VALUES('Another shipper','67856432');

SELECT * FROM Shippers;

INSERT Shippers(ShipperID, CompanyName, Phone) VALUES(4,'Another shipper b','67856432');

SELECT CompanyName, Region FROM Suppliers WHERE Region IS NULL;

SELECT ProductName, UnitPrice, CategoryID FROM Products
ORDER BY CategoryID, UnitPrice DESC;

SELECT DISTINCT Country, City FROM Customers ORDER BY Country;

SELECT CategoryName, 
	CASE CategoryName 
		WHEN 'Beverages' THEN 'Drinks' 
		WHEN 'Condiments' THEN 'Seasonings' 
		WHEN 'Confections' THEN 'Sweets' 
		ELSE 'Something Else' 
	END Comment 
FROM Categories;

SELECT ProductName, UnitPrice,
CASE 
	WHEN UnitPrice > 40 THEN 'Expensive'
    WHEN UnitPrice > 20 THEN 'Reasonable'
    ELSE 'Cheap' END 'Value For Money'
FROM Products;
    
SELECT * FROM Categories;

UPDATE Categories SET CategoryName = 'Beverages' WHERE CategoryName = 'Drinks';

SELECT * FROM Categories WHERE CategoryID != 1 AND CategoryName LIKE 'C%';