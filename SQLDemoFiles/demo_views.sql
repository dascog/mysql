-- 2.  Execute this statement:
DROP VIEW vDemo;

CREATE View vDemo AS
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity FROM Products p JOIN Order_Details od 
	ON p.ProductID = od.ProductID JOIN Orders o 
		ON o.OrderID = od.OrderID 
				ORDER BY ProductName, Quantity DESC;
SELECT * FROM vDemo;

-- Now do the same alteration we did before to display only the top sale of each project
ALTER VIEW vDemo AS
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, 
Quantity FROM Products p JOIN Order_Details od ON p.ProductID = od.ProductID 
JOIN Orders o ON o.OrderID = od.OrderID WHERE Quantity =
(
	SELECT MAX(Quantity) FROM order_details Where ProductID = p.ProductID
);

SELECT * FROM vDemo;