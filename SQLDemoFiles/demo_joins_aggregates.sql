USE Northwind;

-- combine results from two tables using ALIASES for tables (Customers c OR Customers AS c)
-- (this implicitly does an inner join using the WHERE clause)
SELECT CompanyName, OrderID, OrderDate FROM Customers c, Orders o 
WHERE c.CustomerID = o.CustomerID 
AND Country = 'Germany';

-- now if we want to bring in some info about the person who made the sale we can inner join the customers and orders with the employees
SELECT CompanyName, OrderID, OrderDate, CONCAT(FirstName,' ',LastName) `Sales Person` FROM Customers c
JOIN Orders o ON c.CustomerID = o.CustomerID
JOIN Employees e ON o.EmployeeID = e.EmployeeID
WHERE c.Country = 'Germany';

-- a right join (or left join) gives *all* the rows from the right (or left) table even if there are no corresponding entries in the other
SELECT s.CompanyName Supplier, s.Country `Supplier Country`, c.CompanyName Customer,
c.Country `Customer Country` FROM Suppliers s RIGHT JOIN Customers c
ON s.Country = c.Country;

-- a CROSS JOIN gives you every row on the right combined with each row on the left
SELECT CategoryName, ProductName FROM Categories CROSS JOIN Products ORDER BY CategoryName;

SELECT * FROM Employees;

-- JOINS EXERCISE
SELECT CategoryName, CompanyName, ProductName, UnitPrice 
FROM products AS p 
	JOIN Suppliers as s 
    ON s.SupplierID = p.SupplierID 
		JOIN categories AS c 
        ON p.CategoryID = c.CategoryID;

-- you can join a table to itself!
SELECT e.LastName Employee, e.Title, m.LastName Manager, m.Title FROM 
Employees e LEFT JOIN Employees m
ON m.EmployeeID = e.ReportsTo;

-- UNIONS can join result sets with the same number of columns and same datatypes into a single table
SELECT CompanyName, ContactName FROM Customers
UNION
Select CompanyName, ContactName FROM Suppliers;

-- Limit the number of results shown
SELECT ProductName, UnitPrice FROM Products
ORDER BY UnitPrice DESC LIMIT 10;

-- Aggregate Columns using functions
SELECT 	SUM(Quantity) `Total Sales`, 
		COUNT(Quantity) `Num Sales`, 
        AVG(Quantity) `Average Sale`, 
        MIN(Quantity) `Min Sale`, 
        MAX(Quantity) `Max Sale` 
        FROM Order_Details;

-- Exercise Top N
Select ProductName, UnitPrice FROM products ORDER BY UnitPrice DESC LIMIT 10;

-- this returns 89 rows, but lots of NULLS
SELECT Region FROM customers;
SELECT COUNT(*) FROM customers; -- Same count here
SELECT COUNT(Region) FROM customers; -- here you get the count without the NULLS

-- GROUP BY and HAVING
-- 1st do an aggregation on a JOIN:
SELECT ProductName, SUM(Quantity) `Total Sales` FROM Order_Details od
JOIN Products p ON od.ProductID = p.ProductID; 
-- pretty useless outcome ...

-- now add a GROUP BY and things make more sense!
SELECT ProductName, SUM(Quantity) `Total Sales` FROM Order_Details od
JOIN Products p ON od.ProductID = p.ProductID
GROUP BY ProductName;
-- the aggregation for each ProductName

-- Exercise - Aggregates
SELECT CategoryName, SUM(o.Quantity * o.UnitPrice * (1-o.Discount)) 'Total Revenue' 
	FROM categories c JOIN products p 
		ON c.CategoryID = p.CategoryID JOIN order_details o 
			ON o.ProductID = p.ProductID 
	GROUP BY CategoryName;
    
-- You can then select the result using the HAVING clause
SELECT ProductName, SUM(Quantity) `Total Sales` FROM Order_Details od
JOIN Products p ON od.ProductID = p.ProductID
GROUP BY ProductName
HAVING SUM(Quantity) >= 1000;

-- the ROLLUP operator gets us a summary of the GROUP BY categories
USE Northwind;
SELECT CategoryName, ProductName, 
SUM(Quantity) AS Sales, FORMAT(SUM(od.UnitPrice * Quantity),2) AS Revenue
FROM Products p JOIN Order_Details od
ON p.ProductID = od.ProductID JOIN Categories c
ON c.CategoryID = p.CategoryID
GROUP BY CategoryName, ProductName
WITH ROLLUP;

SELECT ProductName, UnitPrice, (SELECT AVG(UnitPrice) FROM Products) Average
FROM Products WHERE UnitPrice <= 
(SELECT AVG(UnitPrice) FROM Products);

-- find all the customer companies residing in the same country as an employee
SELECT CompanyName, Country FROM Customers WHERE Country IN(SELECT DISTINCT Country FROM Employees);

-- EXERCISE - Subqueries
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity FROM Products p JOIN Order_Details od ON p.ProductID = od.ProductID JOIN Orders o ON o.OrderID = od.OrderID ORDER BY ProductName, Quantity DESC;

-- the trick here is to find the Max quantity for all the orders of the productID of the current row ... 
SELECT ProductName, DATE_FORMAT(OrderDate, "%D %b %Y") `Order Date`, Quantity 
	FROM Products p JOIN Order_Details od 
		ON p.ProductID = od.ProductID JOIN Orders o 
			ON o.OrderID = od.OrderID
				WHERE Quantity = (SELECT MAX(Quantity) FROM Order_Details od1 WHERE od1.productID = p.ProductID)
				GROUP BY ProductName
					ORDER BY ProductName, Quantity DESC;

-- CORRELATED SUBQUERIES. Note that the subquery is evaluated for every row in the outer query (not very efficient)
SELECT CategoryName, ProductName, Quantity FROM Categories c JOIN Products p
	ON c.CategoryId = p.CategoryId JOIN Order_Details od 
		ON p.ProductID = od.ProductID 
			WHERE Quantity = (SELECT MAX(Quantity) FROM Categories c1 JOIN Products p1
								ON c1.CategoryId = p1.CategoryId JOIN Order_Details od1 
									ON p1.ProductID = od1.ProductID 
										WHERE c.CategoryID = c1.CategoryId);
     
-- Select all the customer companies from cities in which you have suppliers
USE Northwind;
SELECT CompanyName, Country, City FROM Customers c WHERE EXISTS
(SELECT City FROM Suppliers s WHERE c.City = s.City);
