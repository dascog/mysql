USE Sakila;

-- 1
SELECT first_name, last_name, email from Customer;

-- 2
SELECT DISTINCT city from city;

-- 3
SELECT first_name, last_name, email from Customer WHERE last_name='Lee'; 

-- 4
SELECT first_name, last_name, email from Customer WHERE last_name LIKE 'M%'; 

-- there actually aren't any customers that don't make payments, let's add one in
INSERT INTO Customer(store_id,first_name,last_name,email,address_id) VALUES(1,"DAVID","BOWMAN","DAVID.BOWMAN@sakilacustomer.org",252);

-- 5
-- NOTE This will not return zero for a customer with no payments, you need a join for that.
SELECT c.first_name, c.last_name, SUM(p.amount) AS TotalAmount 
FROM Customer as c, payment as p where p.customer_id = c.customer_id 
GROUP BY c.customer_id ORDER BY TotalAmount;
-- here it is with an outer join
SELECT first_name, last_name, SUM(IFNULL(amount,0)) AS TotalAmount 
FROM Customer as c 
LEFT JOIN payment as p 
ON c.customer_id = p.customer_id 
GROUP BY c.customer_id ORDER BY TotalAmount;

-- 6
SELECT c.first_name, c.last_name 
FROM Customer as c, Payment as p 
WHERE c.customer_id NOT IN (SELECT DISTINCT customer_id FROM Payment) 
GROUP BY c.customer_id;

-- 7
SELECT first_name, last_name, SUM(IFNULL(amount,0)) AS TotalAmount 
FROM Customer as c 
LEFT JOIN payment as p ON c.customer_id = p.customer_id 
GROUP BY c.customer_id 
ORDER BY TotalAmount DESC;

-- 8
SELECT city, COUNT(DISTINCT(customer_id)) as customerCount FROM city as c JOIN address as a ON c.city_id = a.city_id JOIN customer as p ON a.address_id = p.address_id GROUP BY c.city_id ORDER BY customerCount DESC;

-- 9 
SELECT city, COUNT(DISTINCT(customer_id)) AS customerCount 
FROM city as c 
JOIN address as a ON c.city_id = a.city_id 
JOIN customer as p ON a.address_id = p.address_id 
GROUP BY c.city_id 
HAVING customerCount > 1;

-- 10
SELECT c.first_name, c.last_name, p.amount FROM customer as c, payment as p WHERE c.customer_id = p.customer_id AND p.amount = (SELECT MAX(amount) from payment);
