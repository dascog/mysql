# MySQL Foundation Exercises

Before working through the exercises below, familiarize yourself with the structure of the SAKILA data.

1. Produce a report of customer first name, last name and email.
2. Produce a report of the city names, do not show duplicates.
3. Produce a report of customer details for a customer whose last name is Lee.
4. Produce a report of customer details for all customers whose last name starts with ‘M’.
5. Add a new customer into the customer table using 
```INSERT INTO Customer(store_id,first_name,last_name,email,address_id) VALUES(1,"DAVID","BOWMAN","DAVID.BOWMAN@sakilacustomer.org",252);``` 
   Then produce a report with customer first name, last name and total payments.  Report back a 0 if any customer has no sales.
6. Produce a report of customer first name and last name for customers that have no payments.  Do not show customers that have payment data.
7. Produce a report of distinct customer sorted by the payment amount descending.
8. Produce a report of the total number of customers in each city showing city name and the customer count.
9. Produce a report of the cities having more than one customer.
10. [Challenge] The “table” being queried or the value for a WHERE clause can be a query itself.  Provide a list of those customers who bought the most expensive product (i.e. made the most expensive transaction).
